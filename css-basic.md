# What is the box model?
 <All HTML elements can be considered as boxes. In CSS, the term "box model" is used when talking about design and layout.
The CSS box model is essentially a box that wraps around every HTML element. It consists of: margins, borders, padding, and the actual content

# What are block, inline-block and inline elements?

# What do margin and padding mean? What's the difference between the two?

# Units - vh, vw, em, px, rem vs em. When to use each one of them?

# Positioning - absolute, relative, static, fixed. What do they mean and when are they used?

# float - What does this property do?

# Responsive Design - What is Responsive design? What are media queries?

# Layout - How to create layouts using Flex and Grid

# CSS Selectors - When do you apply the following types of selectors?

- type selector
- class selector
- id selector
- child selectors
- descendent selector

# Combining CSS classes - Can you apply two more classes to a single element?


# CSS Cascade, Inheritance and specificity - Which rule wins when multiple rules are applied to a single element?